<?php
error_reporting(E_ERROR | E_PARSE);

function getConnection() {
    $link = mysqli_connect("localhost", "oneap", "S3rvc0rp", "cisco_api_domain");
    mysqli_query($link, "set names 'utf8'");
    if (!$link) {
        if (phpversion() < '4.0')
            exit("Connection Failed: . $php_errormsg");
        else
            exit("Connection Failed:" . odbc_errormsg());
    }
    return $link;
}

function closeConnection($conn) {
    mysqli_close($conn);
}

?>